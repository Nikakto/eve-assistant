from PyQt5 import QtWidgets

import datetime
import logging
import os
import sys

from core.thread import default_threadpool
from gui.views.base import EveAssistantQMainWindow
from memory.tasks import default_worker

# import Qt resources
from gui import resources

try:
    import win32api
except ImportError:
    raise ImportError(
        'Could not import win32api. Install it. '
        'Virtualenv required command "easy_install %pywin_package_name%.exe"'
    )

DEBUG = os.environ.get('DEBUG') == '1'

os.makedirs('./logs', exist_ok=True)
if DEBUG:
    logfile = 'log_debug.txt'
else:
    logfile = f'''log {datetime.date.today().isoformat()}.txt'''

filepath = f'''./logs/{logfile}'''
logging.basicConfig(level=logging.WARN if DEBUG else logging.ERROR, filename=filepath)


class App(QtWidgets.QApplication):
    def __init__(self, *args, **kwargs):
        super(App, self).__init__(*args, **kwargs)
        self.aboutToQuit.connect(default_threadpool.waitForDone)
        self.aboutToQuit.connect(self.kill_tasks_runner)

    @staticmethod
    def kill_tasks_runner():
        default_worker.kill()
        default_worker.join()

    def run(self):
        application = EveAssistantQMainWindow()
        application.show()
        sys.exit(app.exec())


app = App([])


if __name__ == '__main__':
    app.run()
