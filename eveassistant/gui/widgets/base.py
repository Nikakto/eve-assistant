from PyQt5 import QtCore, QtWidgets

from typing import Iterable, List, Tuple, Union

from gui import fonts, palettes


class BaseIcon(QtWidgets.QLabel):

    ICON_CIRCLE = '\uf111'
    ICON_CHECK = '\uf00c'
    ICON_PAUSE = '\uf04c'
    ICON_PLAY = '\uf04b'
    ICON_SYNC_ALT = '\uf2f1'
    ICON_TIMES = '\uf00d'
    ICON_TIMES_CIRCLE = '\uf057'

    def __init__(self, *args):
        super(BaseIcon, self).__init__(*args)
        self.setFont(fonts.FontAwesomeRegular())
        self.setObjectName('BaseIcon')


class BaseQProgressBar(QtWidgets.QProgressBar):
    def __init__(self, *args, **kwargs):
        super(BaseQProgressBar, self).__init__(*args, **kwargs)
        self.setRange(0, 100)
        self.setValue(0)
        self.setFixedHeight(10)
        self.setFixedWidth(100)
        self.setTextVisible(False)


class BaseQLiQWidget(QtWidgets.QWidget):
    def __init__(self, label='', icon=BaseIcon.ICON_CIRCLE, *args):
        super(BaseQLiQWidget, self).__init__(*args)
        self.setObjectName(self.__class__.__name__)
        self.setLayout(QtWidgets.QHBoxLayout())

        self.icon = BaseIcon(icon, self)
        self.layout().addWidget(self.icon)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setAlignment(self.icon, QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeft)

        self.label = QtWidgets.QLabel(label, self)
        self.layout().addWidget(self.label)

        self.setPalette(palettes.PalettePrimary())

    def setPalette(self, QPalette):
        super(BaseQLiQWidget, self).setPalette(QPalette)
        self.icon.setPalette(QPalette)
        self.label.setPalette(QPalette)


class BaseUlQWidget(QtWidgets.QWidget):
    items: List['BaseQLiQWidget']

    def __init__(self, items: Iterable[Union[Tuple[str, str], str]], *args):
        super(BaseUlQWidget, self).__init__(*args)

        self.setLayout(QtWidgets.QVBoxLayout())

        self.items = []
        for item in items:
            args = (item, ) if isinstance(item, str) else item
            self.add_item(*args)

    def add_item(self, label, icon=BaseIcon.ICON_CIRCLE) -> BaseQLiQWidget:
        item = BaseQLiQWidget(label, icon)
        self.items.append(item)
        self.layout().addWidget(item)
        self.layout().setAlignment(item, QtCore.Qt.AlignLeft)
        return item
