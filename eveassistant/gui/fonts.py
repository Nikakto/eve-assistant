from PyQt5 import QtGui


class FontAwesomeBrandsRegular(QtGui.QFont):
    def __init__(self):
        super(FontAwesomeBrandsRegular, self).__init__("Font Awesome 5 Brands Regular")
        self.setPixelSize(32)


class FontAwesomeRegular(QtGui.QFont):
    def __init__(self):
        super(FontAwesomeRegular, self).__init__("Font Awesome 5 Free Regular")
        self.setPixelSize(32)


class FontAwesomeSolid(QtGui.QFont):
    def __init__(self):
        super(FontAwesomeSolid, self).__init__("Font Awesome 5 Free Solid")
        self.setPixelSize(32)
