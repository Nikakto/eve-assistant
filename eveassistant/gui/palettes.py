from PyQt5 import QtGui

from gui import colors


class PaletteDebugBox(QtGui.QPalette):
    def __init__(self, *args, **kwargs):
        super(PaletteDebugBox, self).__init__(*args)
        self.setColor(QtGui.QPalette.Background, colors.Red0)


class PalettePrimary(QtGui.QPalette):
    def __init__(self, *args):
        super(PalettePrimary, self).__init__(*args)
        self.setColor(QtGui.QPalette.Window, colors.Background0)
        self.setColor(QtGui.QPalette.WindowText, colors.Primary)


