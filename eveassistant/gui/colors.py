from PyQt5 import QtGui


Black0 = QtGui.QColor()
Black0.setRgb(0, 0, 0)

Black1 = QtGui.QColor()
Black1.setRgb(10, 10, 10)

Black2 = QtGui.QColor()
Black2.setRgb(20, 20, 20)

Black3 = QtGui.QColor()
Black3.setRgb(30, 30, 30)

Black4 = QtGui.QColor()
Black4.setRgb(40, 40, 40)

Black5 = QtGui.QColor()
Black5.setRgb(50, 50, 50)

Black6 = QtGui.QColor()
Black6.setRgb(60, 60, 60)


Gray0 = QtGui.QColor()
Gray0.setRgb(80, 80, 80)

Gray1 = QtGui.QColor()
Gray1.setRgb(100, 100, 100)

Gray2 = QtGui.QColor()
Gray2.setRgb(120, 120, 120)

Gray3 = QtGui.QColor()
Gray3.setRgb(140, 140, 140)

Gray4 = QtGui.QColor()
Gray4.setRgb(160, 160, 160)

Gray5 = QtGui.QColor()
Gray5.setRgb(180, 180, 180)

Gray6 = QtGui.QColor()
Gray6.setRgb(200, 200, 200)


Red0 = QtGui.QColor()
Red0.setRgb(255, 0, 0)

RedAlpha50 = QtGui.QColor()
RedAlpha50.setRgb(255, 0, 0)
RedAlpha50.setAlpha(100)


White0 = QtGui.QColor()
White0.setRgb(255, 255, 255)

White1 = QtGui.QColor()
White1.setRgb(220, 220, 220)


WhiteAlpha10 = QtGui.QColor()
WhiteAlpha10.setRgb(255, 255, 255, 25)

WhiteAlpha20 = QtGui.QColor()
WhiteAlpha20.setRgb(255, 255, 255, 50)

WhiteAlpha30 = QtGui.QColor()
WhiteAlpha30.setRgb(255, 255, 255, 75)

WhiteAlpha40 = QtGui.QColor()
WhiteAlpha40.setRgb(255, 255, 255, 100)

WhiteAlpha50 = QtGui.QColor()
WhiteAlpha50.setRgb(255, 255, 255, 125)

WhiteAlpha60 = QtGui.QColor()
WhiteAlpha60.setRgb(255, 255, 255, 150)

WhiteAlpha70 = QtGui.QColor()
WhiteAlpha70.setRgb(255, 255, 255, 175)

WhiteAlpha80 = QtGui.QColor()
WhiteAlpha80.setRgb(255, 255, 255, 200)

WhiteAlpha90 = QtGui.QColor()
WhiteAlpha90.setRgb(255, 255, 255, 255)


Background0 = Black2
Background1 = Black4


Primary = White1
Secondary = Gray4
