from PyQt5 import QtCore, QtWidgets

from typing import Tuple, Union

from gui import fonts
from gui.widgets.base import BaseIcon
from memory import process, tasks


class MemoryViewerQWidget(QtWidgets.QWidget):
    table_node_update = QtCore.pyqtSignal()

    def __init__(self, _process: process.EveOnlineProcess, *args, **kwargs):
        super(MemoryViewerQWidget, self).__init__(*args, **kwargs)
        self.process = _process

        layout = QtWidgets.QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        # GUI
        self.tree_bar = MemoryViewerQTreeBarQWidget(parent=self)
        self.layout().addWidget(self.tree_bar, 0, 0)

        self.tree = MemoryViewerQTreeView(self.process, parent=self)
        self.layout().addWidget(self.tree, 1, 0)

        self.table_bar = MemoryViewerQTableBarQWidget(parent=self)
        self.tree_bar.btn_update.clicked.connect(self.update_table_nodes)
        self.layout().addWidget(self.tree_bar, 0, 1)

        self.table = MemoryViewerNodeQTableWidget(self)
        self.layout().addWidget(self.table, 1, 1)
        self.tree.itemSelectionChanged.connect(self.on_tree_item_select)

        # EVENTS
        self.table_node_update.connect(self.table.update_rows)

        self.timer = QtCore.QTimer()
        self.timer.start(1000)
        self.timer.timeout.connect(lambda: None)

    def _update_table_data(self):
        self.table.node.read_children()
        self.table_node_update.emit()

    def on_tree_item_select(self):
        items = self.tree.selectedItems()
        if len(items) == 1:
            item = items[0]
            self.table.node = item.node

    def update_table_nodes(self):
        if self.table.node:
            job = tasks.BlockingJob(self.process, self._update_table_data)
            job.start()


class MemoryViewerQTreeBarQWidget(QtWidgets.QWidget):
    AUTO_UPDATE_RATE = 1000

    def __init__(self, *args, **kwargs):
        super(MemoryViewerQTreeBarQWidget, self).__init__(*args, **kwargs)

        self._auto_update_rate = None

        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignLeft)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self.btn_update = QtWidgets.QPushButton(BaseIcon.ICON_SYNC_ALT)
        self.btn_update.setFont(fonts.FontAwesomeRegular())
        self.btn_update.setFixedHeight(32)
        self.btn_update.setFixedWidth(32)
        self.layout().addWidget(self.btn_update)

        self.btn_auto_update = QtWidgets.QPushButton(BaseIcon.ICON_PLAY)
        self.btn_auto_update.setFont(fonts.FontAwesomeSolid())
        self.btn_auto_update.setFixedHeight(32)
        self.btn_auto_update.setFixedWidth(32)
        self.btn_auto_update.clicked.connect(self.auto_update_toggle)
        self.layout().addWidget(self.btn_auto_update)

        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.emit_update_event)

    @property
    def auto_update_enabled(self):
        return self._auto_update_rate is not None

    @auto_update_enabled.setter
    def auto_update_enabled(self, value):
        self._auto_update_rate = self.AUTO_UPDATE_RATE if value else None
        if self.auto_update_enabled:
            if not self.timer.isActive():
                self.timer.start(self._auto_update_rate)
        else:
            self.timer.stop()

        self.btn_auto_update.setText(BaseIcon.ICON_PAUSE if self.auto_update_enabled else BaseIcon.ICON_PLAY)

    def auto_update_toggle(self):
        self.auto_update_enabled = not self.auto_update_enabled

    def emit_update_event(self):
        self.btn_update.clicked.emit()
        if self.auto_update_enabled:
            self.timer.start(self._auto_update_rate)


class MemoryViewerQTableBarQWidget(MemoryViewerQTreeBarQWidget):
    pass


class MemoryViewerQTreeView(QtWidgets.QTreeWidget):
    HEADER = ('Node', 'Type', 'Address')
    NODE_NAME_INDEX = HEADER.index('Node')
    NODE_TYPE_INDEX = HEADER.index('Type')
    NODE_ADDRESS_IDNEX = HEADER.index('Address')

    def __init__(self, _process: process.EveOnlineProcess, *args, **kwargs):
        super(MemoryViewerQTreeView, self).__init__(*args, **kwargs)

        self.itemExpanded.connect(MemoryViewerQTreeWidgetItem.children_read)

        self.setHeaderLabels(self.HEADER)
        self.setColumnWidth(0, 200)
        self.setColumnWidth(1, 100)
        self.setColumnWidth(2, 100)

        self.top_level_item = MemoryViewerQTreeWidgetItem(_process.uiroot, ['UIRoot', _process.uiroot.type, hex(_process.uiroot.address).upper()])
        self.top_level_item.set_expandable()
        self.addTopLevelItem(self.top_level_item)


class MemoryViewerQTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, node: process.nodes.Node = None, *args):
        super(MemoryViewerQTreeWidgetItem, self).__init__(*args)

        self.children = []
        self.node = node

    def _get_nodes_diff(self):
        exists = {child.text(MemoryViewerQTreeView.NODE_NAME_INDEX): child.node for child in self.children}
        to_create = [node_name for node_name, node in self.node.children.items() if node.has_children() and not exists.get(node_name)]
        to_update = [node_name for node_name, node in self.node.children.items() if node.has_children() and exists.get(node_name) != node]
        return to_create, to_update, list(set(exists) - set(self.node.children))

    def children_create(self, node_names):
        for node_name in node_names:
            node = self.node.children.get(node_name)
            child = MemoryViewerQTreeWidgetItem(node, [str(node_name), node.type, hex(node.address).upper()])
            child.set_expandable()
            self.children.append(child)

    def children_remove(self, to_remove=None):
        for child in self.children:
            if to_remove is None or child.text(MemoryViewerQTreeView.NODE_NAME_INDEX) in to_remove:
                self.children.remove(child)
                self.removeChild(child)

    def children_update(self, node_names):
        for node_name in node_names:
            node = self.node.children.get(node_name)
            child = self.get_child_by_name(node_name)
            child.node = node
            child.update()

    def children_read(self):
        self.node.read_children()
        if not (self.node and self.node.children):
            self.children_remove()
            return

        to_create, to_update, to_remove = self._get_nodes_diff()
        self.children_create(to_create)
        self.children_update(to_update)
        self.children_remove(to_remove)

        self.addChildren(self.children)
        self.sortChildren(0, QtCore.Qt.SortOrder(0))

    def get_child_by_name(self, name) -> 'MemoryViewerQTreeWidgetItem':
        for child in self.children:
            if child.text(MemoryViewerQTreeView.NODE_NAME_INDEX) == name:
                return child

    def set_expandable(self):
        if self.node and self.node.size:
            self.children = [MemoryViewerQTreeWidgetItem()]
            self.addChildren(self.children)
            return

    def update(self):
        self.setText(MemoryViewerQTreeView.NODE_TYPE_INDEX, self.node.type)
        self.setText(MemoryViewerQTreeView.NODE_ADDRESS_IDNEX, hex(self.node.address).upper())


class MemoryViewerNodeQTableWidget(QtWidgets.QTableWidget):
    _node: Union['process.nodes.Node', None]
    LABELS_HEADER = ['Property', 'Value', 'Type', 'Address']

    def __init__(self, *args):
        super(MemoryViewerNodeQTableWidget, self).__init__(*args)
        self._node = None

        self.setMinimumWidth(450)
        self.setMaximumWidth(450)
        self.verticalHeader().hide()

        self.setColumnCount(4)
        self.setColumnWidth(0, 150)
        self.setColumnWidth(1, 75)
        self.setColumnWidth(2, 75)
        self.setColumnWidth(3, 130)

        self.setHorizontalHeaderLabels(self.LABELS_HEADER)

    @property
    def node(self) -> Union['process.nodes.Node', None]:
        return self._node

    @node.setter
    def node(self, node: 'process.nodes.Node'):
        self._node = node
        self.update_rows()

    @staticmethod
    def make_row(item: Tuple[str, 'process.nodes.Node']):
        prop_name, prop = item
        return prop_name, str(prop.value), prop.type, hex(prop.address).upper()

    def update_rows(self):
        self.setSortingEnabled(False)
        while self.rowCount():
            self.removeRow(0)

        if not self.node:
            return

        items = ((node_name, node) for node_name, node in self.node.children.items() if not node.children)
        for row_index, item in enumerate(items):
            self.insertRow(self.rowCount())
            self.setRowHeight(row_index, 10)
            for col_index, data in enumerate(self.make_row(item)):
                self.setItem(row_index, col_index, QtWidgets.QTableWidgetItem(data))

        self.setSortingEnabled(True)
        self.sortItems(0)
        self.update()
