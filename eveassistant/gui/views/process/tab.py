from PyQt5 import QtCore, QtGui, QtWidgets

import logging
import os
from dataclasses import dataclass
from typing import Iterable, List

from core.thread import default_threadpool, Worker
from gui import colors, fonts, palettes
from gui.views.process.ingame.inflight import InFlightViewerQWidget
from gui.views.process.memory import MemoryViewerQWidget
from gui.widgets.base import BaseIcon, BaseQProgressBar, BaseQLiQWidget, BaseUlQWidget
from memory import process


logger = logging.getLogger("eve_assistant")


class EveOnlineProcessesQTabWidget(QtWidgets.QTabWidget):
    processes = QtCore.pyqtSignal(list, list)

    def __init__(self, *args, **kwargs):
        super(EveOnlineProcessesQTabWidget, self).__init__(*args, **kwargs)

        self.setMinimumWidth(1000)

        self.processes.connect(self.processes_update_tabs)
        self.repeater_processes = default_threadpool.loop(Worker(self.processes_find), times=float('inf'), delay=1000)

    def _tabs_add(self, processes: Iterable['process.EveOnlineProcess']):
        for process in processes:
            logger.info(f'''EveOnline {process.pid=} is new ''', )
            self.addTab(EveOnlineProcessQTabWidget(process), process.charname)

    def _tabs_remove(self, processes: Iterable['process.EveOnlineProcess']):
        index_to_remove = (index for index in range(self.count()) if self.widget(index).process in processes)
        for index in index_to_remove:
            widget = self.widget(index)
            logger.info(f'''EveOnline process.pid=({widget.process.pid}) is dead ''')
            self.removeTab(index)

    def processes_find(self):
        logger.info(f'''EveOnline processes search...''')
        if (diff := process.EveOnlineProcess.get_processes_diff()) and any(diff):
            self.processes.emit(*diff)
            logger.info(f'''EveOnline processes find diff''')

    def processes_update_tabs(self, new: List['process.EveOnlineProcess'], dead: List['process.EveOnlineProcess']):
        self._tabs_add(new)
        self._tabs_remove(dead)


class EveOnlineProcessQTabWidget(QtWidgets.QTabWidget):
    process_uiroot_finder = None

    def __init__(self, _process: process.EveOnlineProcess, *args, **kwargs):
        super(EveOnlineProcessQTabWidget, self).__init__(*args, **kwargs)
        self.process = _process

        self.setLayout(QtWidgets.QGridLayout())
        self.memory_viewer = None

        if self.process.uiroot:
            self._init_view()
        else:
            self.get_uiroot()

    def get_uiroot(self):
        self.process_uiroot_finder = EveOnlineProcessUIRootLookupQWidget()
        self.layout().addWidget(self.process_uiroot_finder)
        self._get_uiroot_class_names_address()

    def _init_view(self):
        while self.layout().count():
            child = self.layout().takeAt(0)
            if child.widget():
                child.widget().deleteLater()
        self.ingame_viewer = InFlightViewerQWidget(self.process)
        self.memory_viewer = MemoryViewerQWidget(self.process)
        self.addTab(self.ingame_viewer, 'ingame')
        self.addTab(self.memory_viewer, 'memory')

    def _get_uiroot_class_names_address(self):
        self._run_step(self.process.find_uiroot_class_name, worker_next_function=self._get_uiroot_class_type_address)

    def _get_uiroot_class_instance_address(self, addresses_of_class: List[int]):
        self._run_step(self.process.find_uiroot_instance, addresses_of_class, worker_next_function=self._init_view)

    def _get_uiroot_class_type_address(self, addresses_of_class: List[int]):
        self._run_step(self.process.find_uiroot_class_type, addresses_of_class, worker_next_function=self._get_uiroot_class_instance_address)

    def _run_step(self, worker_function, *args, worker_next_function=None, **kwargs):
        worker = Worker(worker_function, *args, **kwargs)
        if worker_next_function:
            worker.signals.result.connect(worker_next_function)
        if worker_next_function != self._init_view:
            worker.signals.finished.connect(self.process_uiroot_finder.step_next)
        default_threadpool.start(worker)


class EveOnlineProcessUIRootLookupQWidget(QtWidgets.QWidget):
    step_active: 'EveOnlineProcessUIRootStep' = None

    def __init__(self, *args, **kwargs):
        super(EveOnlineProcessUIRootLookupQWidget, self).__init__(*args, **kwargs)

        self.setLayout(QtWidgets.QVBoxLayout())

        self.progress_bar = BaseQProgressBar()
        self.layout().addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
        self.layout().addWidget(self.progress_bar)
        self.layout().setAlignment(self.progress_bar, QtCore.Qt.AlignCenter)
        self.startTimer(100)

        self.steps = [
            EveOnlineProcessUIRootStep(code='IURootClassName', description='Searching interface class description'),
            EveOnlineProcessUIRootStep(code='IURootClassType', description='Searching interface class type'),
            EveOnlineProcessUIRootStep(code='IURootClassInstance', description='Searching active interface instance'),
        ]

        self.list = BaseUlQWidget([])
        self.layout().addWidget(self.list)
        self.layout().setAlignment(self.list, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
        self.init_list()

        self.layout().addItem(QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))

    def timerEvent(self, *args, **kwargs):
        if self.progress_bar.value() >= self.progress_bar.maximum():
            self.progress_bar.setValue(0)
        else:
            self.progress_bar.setValue(self.progress_bar.value() + 1)

    def init_list(self):
        for step in self.steps:
            step.item = self.list.add_item(label=step.description, icon=step.icon)
        self.step_next()

    def step_next(self):
        index = (self.steps.index(self.step_active) + 1) if self.step_active else 0
        self.step_active = self.steps[index]
        self.update_list()

    def update_list(self):
        for step in self.steps:
            self.update_step(step)

    def update_step(self, step):
        if self.steps.index(step) < self.steps.index(self.step_active):
            step.set_style_done()
        elif self.step_active is step and step.error:
            step.set_style_error()
        elif self.step_active is step:
            step.set_style_active()
        else:
            step.set_style_pending()


@dataclass
class EveOnlineProcessUIRootStep:
    PALETTE_ACTIVE = palettes.PalettePrimary()
    PALETTE_ERROR = palettes.PalettePrimary()
    PALETTE_ERROR.setColor(QtGui.QPalette.WindowText, colors.White0)
    PALETTE_PENDING = palettes.PalettePrimary()
    PALETTE_PENDING.setColor(QtGui.QPalette.WindowText, colors.WhiteAlpha50)

    ICON_DONE = BaseIcon.ICON_CHECK
    ICON_ERROR = BaseIcon.ICON_TIMES
    ICON_PENDING = BaseIcon.ICON_CIRCLE

    code: str
    description: str
    icon: str = ICON_PENDING
    active: bool = False
    error: bool = False
    font_icon: QtGui.QFont = fonts.FontAwesomeRegular()
    item: BaseQLiQWidget = None
    palette: QtGui.QPalette = PALETTE_PENDING

    def update(self, active, icon, icon_font, palette):
        self.active = active

        self.palette = palette
        self.item.setPalette(palette)

        self.font_icon = icon_font
        self.item.icon.setFont(self.font_icon)

        self.icon = icon
        self.item.icon.setText(self.icon)

        self.item.label.setText(f'{self.description}...' if self.active else self.description)

    def set_style_active(self):
        self.update(active=True, icon=self.ICON_PENDING, icon_font=fonts.FontAwesomeRegular(), palette=self.PALETTE_ACTIVE)

    def set_style_done(self):
        self.update(active=False, icon=self.ICON_DONE, icon_font=fonts.FontAwesomeRegular(), palette=self.PALETTE_PENDING)

    def set_style_error(self):
        self.update(active=False, icon=self.ICON_ERROR, icon_font=fonts.FontAwesomeSolid(), palette=self.PALETTE_ERROR)

    def set_style_pending(self):
        self.update(active=False, icon=self.ICON_PENDING, icon_font=fonts.FontAwesomeRegular(), palette=self.PALETTE_PENDING)

