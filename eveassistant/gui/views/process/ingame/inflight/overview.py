from PyQt5 import QtCore, QtWidgets

import random
from typing import List, Union

from gui.views.process.ingame.decorators import ingame_object_required
from gui.views.process.ingame.generic import menus, mixins
from gui.views.process.ingame.generic.objects import AutoUpdateQTableWidget
from memory.ingame.inflight.overview import OverView, OverViewContentItem


class OverViewQMenu(QtWidgets.QMenu):
    def __init__(self, *args):
        super(OverViewQMenu, self).__init__(*args)

        self.debug = menus.DebugQMenu()
        self.addMenu(self.debug)


class OverViewQTableWidget(mixins.ContextMenuMixin,
                           mixins.OverlayDebugMixin,
                           mixins.InGameMixin,
                           AutoUpdateQTableWidget):

    menu_class = OverViewQMenu
    menu_debug = True

    COLUMNS = (
        {'label': '', 'width': 16},
        {'label': 'Distance', 'width': 100},
        {'label': 'Velocity', 'width': 80},
        {'label': 'Type', 'width': 60},
        {'label': 'Name', 'width': 100},
    )

    def get_ingame_object(self) -> Union['OverView', None]:
        return (self._process.uiroot.ingame.main and
                self._process.uiroot.ingame.main.inflight_overview and
                self._process.uiroot.ingame.main.inflight_overview.tab_group and
                self._process.uiroot.ingame.main.inflight_overview.tab_group.panel and
                self._process.uiroot.ingame.main.inflight_overview.tab_group.panel.content) or None

    @ingame_object_required
    def get_rows(self):
        if not self.ingame_object:
            return []
        return [(None, item.distance_formatted(item.distance), None, None, item.name) for item in self.ingame_object.items]

    # @ingame_object_required
    # def select_random(self):
    #     index = random.randint(0, len(self.tab_selected.items) - 1)
    #     item = self.tab_selected.items[index]
    #     print('select', item)
    #     item.click()
    #     item.update()
    #     print('selected', item.selected)


class OverViewItemQTableWidgetItem(mixins.ContextMenuMixin,
                                   mixins.OverlayDebugMixin,
                                   QtWidgets.QTableWidgetItem):

    menu_class = menus.DebugQMenu
    menu_debug = True

    ingame_object: 'OverViewContentItem' = None

    def __init__(self, ingame_object: OverViewContentItem, attr: Union[str, None] = None, *args, **kwargs):
        super(OverViewItemQTableWidgetItem, self).__init__(*args, **kwargs)
        self.ingame_object = ingame_object
        self.attr = attr

