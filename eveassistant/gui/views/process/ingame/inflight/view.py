from PyQt5 import QtWidgets

from .overview import OverViewQTableWidget
from .selecteditemview import SelectedItemViewQWidget
from memory import process


class InFlightViewerQWidget(QtWidgets.QWidget):
    def __init__(self, _process: 'process.EveOnlineProcess', *args, **kwargs):
        super(InFlightViewerQWidget, self).__init__(*args, **kwargs)
        self.process = _process

        layout = QtWidgets.QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        # self.location = QtWidgets.QWidget(parent=self)
        # self.location.setFixedHeight(100)
        # self.location.setFixedWidth(400)
        # self.layout().addWidget(self.location, 0, 0)
        #
        # self.probe_scanner = QtWidgets.QWidget(parent=self)
        # self.probe_scanner.setFixedHeight(100)
        # self.probe_scanner.setFixedWidth(1000)
        # self.layout().addWidget(self.probe_scanner, 1, 0)
        #
        # self.selected = QtWidgets.QWidget(parent=self)
        # self.selected.setFixedHeight(100)
        # self.layout().addWidget(self.selected, 0, 1)

        self.selected_item = SelectedItemViewQWidget(_process, self)
        self.layout().addWidget(self.selected_item, 0, 1)

        self.overview = OverViewQTableWidget(_process, self)
        self.layout().addWidget(self.overview, 1, 1)
