from PyQt5 import QtWidgets

from typing import Union

from gui.views.process.ingame.generic import menus, mixins, objects
from memory import ingame


class SelectedItemViewQMenu(QtWidgets.QMenu):
    def __init__(self, *args):
        super(SelectedItemViewQMenu, self).__init__(*args)

        self.debug = menus.DebugQMenu(self)
        self.addMenu(self.debug)


class SelectedItemViewQWidget(mixins.ContextMenuMixin,
                              mixins.OverlayDebugMixin,
                              mixins.InGameMixin,
                              QtWidgets.QWidget):

    menu_class = SelectedItemViewQMenu
    menu_debug = True

    def __init__(self, *args, **kwargs):
        super(SelectedItemViewQWidget, self).__init__(*args, **kwargs)

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        self.setFixedHeight(150)
        self.setFixedWidth(300)

    def get_ingame_object(self) -> Union['ingame.VisibleInGameObject', None]:
        ingame_main = self._process.uiroot.ingame.main
        return ingame_main.inflight_selected_item_view if ingame_main else None
