from functools import wraps
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from .generic.mixins import InGameMixin


def ingame_object_required(func):
    @wraps(func)
    def wrapper(self: 'InGameMixin', *args, **kwargs):
        if self.ingame_object:
            return func(self, *args, **kwargs)
    return wrapper
