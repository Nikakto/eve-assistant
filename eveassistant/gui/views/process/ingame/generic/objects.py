from PyQt5 import QtCore, QtWidgets

from typing import Dict, Iterable, Tuple, Union, TYPE_CHECKING

from gui.palettes import PaletteDebugBox
from gui.views.process.ingame.generic import mixins
from memory.ingame.scroll import ScrollAreaInGameObject
from memory.ingame.tabs import TabGroupPanelInGameObject


if TYPE_CHECKING:
    from memory.ingame.tabs import TabGroupInGameObject, TabGroupPanelInGameObject


class DebugOverlay(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(DebugOverlay, self).__init__(*args, **kwargs)

        self.setPalette(PaletteDebugBox())
        self.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents, True)
        self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint)
        self.setWindowOpacity(0.5)
        self.setVisible(False)


class ScrollAreaViewQWidget(mixins.InGameMixin, QtWidgets.QWidget):
    def get_ingame_object(self) -> Union['ScrollAreaInGameObject', None]:
        if ingame_parent := self.parent().ingame_object:
            return ingame_parent.scroll_area and ingame_parent.scroll_area.content
        return None


class TabGroupPanelViewQWidget(ScrollAreaViewQWidget):
    def get_ingame_object(self) -> Union['TabGroupPanelInGameObject', None]:
        if ingame_parent := self.parent().ingame_object:
            return ingame_parent.panel and ingame_parent.panel.content
        return None


class TabGroupViewQWidget(mixins.InGameMixin, QtWidgets.QWidget):
    panel_class = TabGroupPanelViewQWidget
    # tabs_class = TabGroupTabDataInGameObject

    def __init__(self, *args, panel_class=None, tabs_class=None, **kwargs):
        super(TabGroupViewQWidget, self).__init__(*args, **kwargs)

        panel_class = panel_class or self.panel_class
        self.panel = panel_class(_process=self._process, parent=self)

        # tabs_class = tabs_class or self.tabs_class
        # self.tabs = tabs_class(parent=self, _process=self._process)

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        # self.layout().addWidget(self.tabs)
        self.layout().addWidget(self.panel)

    def get_ingame_object(self) -> Union['TabGroupInGameObject', None]:
        if ingame_parent := self.parent().ingame_object:
            return ingame_parent.tab_group
        return None


class AutoUpdateQTableWidget(QtWidgets.QTableWidget):
    COLUMNS: Tuple[Dict] = tuple()

    def __init__(self, *args):
        super(AutoUpdateQTableWidget, self).__init__(*args)

        self.setSortingEnabled(False)
        self.setColumnCount(len(self.COLUMNS))
        self.setHorizontalHeaderLabels([column.get('label', 'unnamed') for column in self.COLUMNS])
        for index, column in enumerate(self.COLUMNS):
            self.setColumnWidth(index, column.get('width'))

        self.verticalHeader().hide()
        self.update()

        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self._update_rows)
        self._update_rows()

    def _update_rows(self):
        self.update_rows()
        self.timer.start(1000)

    def insert_row_cells(self, columns_data: Iterable[str]):
        self.insertRow(self.rowCount())
        for index, data in enumerate(columns_data):
            self.setItem(self.rowCount() - 1, index, QtWidgets.QTableWidgetItem(data))

    def get_rows(self) -> Iterable:
        return []

    def update_rows(self):
        self.setRowCount(0)
        items = self.get_rows()
        if not items:
            return
        for row in items:
            self.insert_row_cells(row)
