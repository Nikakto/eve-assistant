from PyQt5 import QtCore, QtGui, QtWidgets

from typing import Union, TYPE_CHECKING

from gui.views.process.ingame.decorators import ingame_object_required
from memory.ingame import AbstractInGameObject, VisibleInGameObject


if TYPE_CHECKING:
    from .menus import DebugQMenu


class ContextMenuMixin:
    menu_class = QtWidgets.QMenu
    menu_debug = False

    def __init__(self, *args, **kwargs):
        super(ContextMenuMixin, self).__init__(*args, **kwargs)
        self.menu = self.menu_class()

        if self.menu_debug:
            if not hasattr(self, 'menu_connect_debug'):
                raise AttributeError(f'''{self.__class__.__name__} trying to connect debug handler. Required debug mixin''')
            self.menu_connect_debug(self.menu.debug)

    def context_menu_open(self):
        if self.menu:
            self.menu.exec_(QtGui.QCursor().pos())

    def mousePressEvent(self, mouse_event):
        if mouse_event.button() == QtCore.Qt.RightButton:
            self.context_menu_open()


class InGameMixin:
    def __init__(self, _process: 'VisibleInGameObject', *args, **kwargs):
        self._process = _process  # must set process before continue init
        super(InGameMixin, self).__init__(*args, **kwargs)
        if not self._process.uiroot:
            raise ValueError('Eve Online Process return None for uiroot')

    def get_ingame_object(self) -> Union['AbstractInGameObject', None]:
        raise NotImplementedError()

    @property
    def ingame_object(self) -> Union['AbstractInGameObject', None]:
        return self.get_ingame_object()


class OverlayDebugMixin:
    object: VisibleInGameObject = None

    def __init__(self, *args, **kwargs):
        from gui.views.process.ingame.generic.objects import DebugOverlay

        super(OverlayDebugMixin, self).__init__(*args, **kwargs)
        self.overlay_debug = DebugOverlay()
        self.overlay_debug_timer = QtCore.QTimer()
        self.overlay_debug_timer.setSingleShot(True)

    @ingame_object_required
    def _debug_overlay_box_inner(self):
        x_shift, y_shift = self._process.ingame_pos
        self.debug_overlay_change(self.ingame_object.content_x + x_shift,
                                  self.ingame_object.content_y + y_shift,
                                  self.ingame_object.content_width,
                                  self.ingame_object.content_height)

    @ingame_object_required
    def _debug_overlay_box_outer(self):
        x_shift, y_shift = self._process.ingame_pos
        self.debug_overlay_change(self.ingame_object.x + x_shift,
                                  self.ingame_object.y + y_shift,
                                  self.ingame_object.width,
                                  self.ingame_object.height)

    def _debug_overlay_toggle(self, state, callback):
        self.overlay_debug.setVisible(state)

        if not state and self.overlay_debug_timer.isActive():
            self.overlay_debug_timer.timeout.disconnect()
            self.overlay_debug_timer.stop()
            return

        self.overlay_debug_timer.timeout.connect(callback)
        self.overlay_debug_timer.start(1000)

    def debug_overlay_change(self, x: int, y: int, width: int, height: int):
        self.overlay_debug.move(x, y)
        self.overlay_debug.setFixedHeight(height)
        self.overlay_debug.setFixedWidth(width)
        self.overlay_debug_timer.start(1000)

    def menu_connect_debug(self, menu: 'DebugQMenu'):
        menu.debug_box_inner.connect(self.debug_overlay_toggle_box_inner)
        menu.debug_box_outer.connect(self.debug_overlay_toggle_box_outer)

    def debug_overlay_toggle_box_inner(self, state):
        self._debug_overlay_toggle(state, callback=self._debug_overlay_box_inner)

    def debug_overlay_toggle_box_outer(self, state):
        self._debug_overlay_toggle(state, callback=self._debug_overlay_box_outer)
