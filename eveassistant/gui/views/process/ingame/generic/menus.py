from PyQt5 import QtCore, QtWidgets

from typing import Callable


class DebugQMenu(QtWidgets.QMenu):
    debug_box_inner = QtCore.pyqtSignal(bool)
    debug_box_outer = QtCore.pyqtSignal(bool)

    def __init__(self, *args):
        super(DebugQMenu, self).__init__(*args)
        self.debug = self

        self.setTitle('Debug')
        self.action_inner = self._add_action('Box [Inner]', self.debug_box_inner_toggle)
        self.action_outer = self._add_action('Box [Outer]', self.debug_box_outer_toggle)

    def _add_action(self, label: str, callback: Callable):
        action = self.addAction(label)
        action.setCheckable(True)
        action.setChecked(False)
        action.setText(label)
        action.triggered.connect(callback)
        return action

    def disable_all(self):
        self.action_inner.setChecked(False)
        self.debug_box_inner.emit(False)

        self.action_outer.setChecked(False)
        self.debug_box_outer.emit(False)

    def debug_box_inner_toggle(self):
        state = self.action_inner.isChecked()
        self.disable_all()
        self.action_inner.setChecked(state)
        self.debug_box_inner.emit(state)

    def debug_box_outer_toggle(self):
        state = self.action_outer.isChecked()
        self.disable_all()
        self.action_outer.setChecked(state)
        self.debug_box_outer.emit(state)
