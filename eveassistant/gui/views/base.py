from PyQt5 import QtGui, QtWidgets

import logging
from core.exceptions import ResourceError
from gui.views.process.tab import EveOnlineProcessesQTabWidget


logger = logging.getLogger("eve_assistant")


class EveAssistantQMainWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(EveAssistantQMainWindow, self).__init__(*args, **kwargs)

        self.add_font(":/fonts/FontAwesome.otf")
        self.add_font(":/fonts/FontAwesome-brands.otf")
        self.add_font(":/fonts/FontAwesome-solid.otf")

        self.setCentralWidget(EveOnlineProcessesQTabWidget(self))
        self.setWindowTitle('Eve Assistant')

        with open('./styles/styles.css', "r") as styles:
            self.setStyleSheet(styles.read())

    def add_font(self, resource):
        if QtGui.QFontDatabase.addApplicationFont(resource) < 0:
            raise ResourceError(f'''Cannot find {resource}''')
