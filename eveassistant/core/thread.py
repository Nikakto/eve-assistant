from PyQt5 import QtCore

from functools import partial
import logging
import traceback
import sys
from typing import Callable


logger = logging.getLogger("eve_assistant")


class ExtendedQThreadPool(QtCore.QThreadPool):
    def __init__(self, parent=None):
        super(ExtendedQThreadPool, self).__init__(parent=None)
        self.timer = QtCore.QTimer()

    def delay(self, m_sec, QRunnable, priority=0):
        priority = priority or (hasattr(QRunnable, 'priority') and QRunnable.priority)
        func = partial(self.start, QRunnable, priority)
        self.timer.singleShot(m_sec, func)

    def loop(self, QRunnable, times=1, delay=1000, priority=0):
        repeater = Repeater(QRunnable, times=times, delay=delay, threadpool=self, priority=priority)
        repeater.worker.signals.finished.connect(repeater.repeat)
        self.start(repeater)
        return repeater


# WARN: define before set as default value to Worker
default_threadpool = ExtendedQThreadPool()
default_threadpool.setMaxThreadCount(1)


class WorkerSignals(QtCore.QObject):
    finished = QtCore.pyqtSignal()
    error = QtCore.pyqtSignal(tuple)
    result = QtCore.pyqtSignal(object)


class Worker(QtCore.QRunnable):

    def __init__(self, func: Callable, *args, **kwargs):
        super(Worker, self).__init__()

        self.func = func
        self.args = args
        self.kwargs = kwargs

        self.setAutoDelete(False)

        self.signals = WorkerSignals()
        self.signals.error.connect(self.error_log)

    @staticmethod
    def error_log(error):
        exctype, value, traceback = error
        logger.error(traceback)
        traceback.print_exc(traceback)

    @QtCore.pyqtSlot()
    def run(self):
        try:
            result = self.func(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
        finally:
            self.signals.finished.emit()


class Repeater(QtCore.QRunnable):

    def __init__(self, worker: Worker, times=1, delay=1000, threadpool=default_threadpool, priority=0, *args, **kwargs):
        super(Repeater, self).__init__(*args, **kwargs)
        self.delay = delay
        self.priority = priority
        self._stop = True
        self.times = times
        self.threadpool = threadpool
        self.worker = worker

        self.setAutoDelete(False)

    @QtCore.pyqtSlot()
    def run(self):
        self._stop = False
        self.worker.run()

    def repeat(self):
        self.times -= 1
        if not self._stop and self.times > 0:
            self.threadpool.delay(self.delay, self)

    def stop(self):
        self._stop = True
