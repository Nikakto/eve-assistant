from memory.ingame.generic import AbstractInGameObject
from memory.ingame.inflight.overview import OverView
from memory.ingame.inflight.selecteditemview import InflightSelectedItemView
from memory.ingame.viewstate import ViewStates


class Main(AbstractInGameObject):
    job_repeat = float('inf')
    job_delay_seconds = 1

    inflight_selected_item_view: 'InflightSelectedItemView' = None
    inflight_overview: OverView = None

    @property
    def map(self):
        return {
            'children': {
                'overview': ('inflight_overview', OverView, {self.CHILD_OPTIONAL: True}),
                'selecteditemview': ('inflight_selected_item_view', InflightSelectedItemView, {self.CHILD_OPTIONAL: True}),
            }
        }

    def kill(self):
        self.inflight_overview.kill()
        self.inflight_selected_item_view.kill()
        super(Main, self).kill()


class UIRoot(AbstractInGameObject):
    job_repeat = float('inf')
    job_delay_seconds = 5

    main: 'Main' = None
    view_state: 'ViewStates' = None

    @property
    def map(self):
        return {
            'children': {
                'l_main': ('main', Main),
                'l_viewstate': ('view_state', ViewStates)
            }
        }

    def kill(self):
        self.main.kill()
        self.view_state.kill()
        super(UIRoot, self).kill()
