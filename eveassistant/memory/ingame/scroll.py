import logging

from .generic import VisibleInGameObject
from memory.nodes import NodeInt


logger = logging.getLogger("eve_assistant")


class ScrollAreaAbstractContainerInGameObject(VisibleInGameObject):
    ingame_parent: 'ScrollAreaInGameObject'

    @property
    def x(self) -> int:
        return self.ingame_parent.x + self._x if self.ingame_parent else self._x

    @property
    def y(self) -> int:
        return self.ingame_parent.y + self._y if self.ingame_parent else self._y


class ScrollAreaClipperInGameObject(ScrollAreaAbstractContainerInGameObject):
    pass


class ScrollAreaContentInGameObject(ScrollAreaAbstractContainerInGameObject):
    @property
    def x(self) -> int:
        return self.ingame_parent.clipper.content_x + NodeInt.as_value_int32(self._x)

    @property
    def y(self) -> int:
        return self.ingame_parent.clipper.content_y + NodeInt.as_value_int32(self._y)


class ScrollAreaInGameObject(VisibleInGameObject):
    clipper_class = ScrollAreaClipperInGameObject
    clipper: clipper_class = None

    content_class = ScrollAreaContentInGameObject
    content: content_class = None

    @property
    def map(self):
        return self.merge_maps(super(ScrollAreaInGameObject, self).map, {
            '_sr': {
                'clipper': ('clipper', self.clipper_class),
                'content': ('content', self.content_class),
            }
        })

    @property
    def content_height(self) -> int:
        return self.content.height if self.content else 0

    @property
    def content_x(self) -> int:
        return self.content.x if self.content else 0

    @property
    def content_y(self) -> int:
        return self.content.y if self.content else 0

    @property
    def content_width(self) -> int:
        return self.content.width if self.content else 0

    def update(self) -> None:
        super(ScrollAreaInGameObject, self).update()
        self.clipper.update()
        self.content.update()
