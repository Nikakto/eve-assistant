from typing import List, TYPE_CHECKING

from memory.ingame.generic import WindowInGameObject
from memory import nodes, process, tasks


if TYPE_CHECKING:
    from memory.ingame.generic import AbstractInGameObject


class InflightSelectedItemView(WindowInGameObject):
    job_repeat = float('inf')
    job_delay_seconds = 1
