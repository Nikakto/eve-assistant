from typing import List

from memory.ingame import generic, scroll, tabs
from memory.tasks import BlockingJob


class OverView(generic.WindowInGameObject):
    job_repeat = float('inf')
    job_delay_seconds = 1

    tab_group: 'OverViewTabGroupView' = None

    @property
    def map(self):
        return self.merge_maps(super(OverView, self).map, {
            '_sr': {
                'main': {
                    'children': {
                        'tabparent': ('tab_group', OverViewTabGroupView)
                    }
                }
            }
        })

    def kill(self):
        super(OverView, self).kill()


class OverViewContent(scroll.ScrollAreaContentInGameObject):
    items: List['OverViewContentItem']

    def __init__(self, *args, **kwargs):
        super(OverViewContent, self).__init__(*args, **kwargs)
        self.items = []

    @property
    def map(self):
        return self.merge_maps(super(OverViewContent, self).map, {
            'children': {
                self.CHILD_LIST: ('items', OverViewContentItem)
            }
        })

    def update(self) -> None:
        super(OverViewContent, self).update()
        for item in self.items:
            item.update()


class OverViewContentItem(generic.VisibleInGameObject):
    job_repeat = float('inf')
    job_delay_seconds = 1

    name: str = ''
    item_id: int = 0
    distance: float = 0
    selected: bool = False

    @property
    def map(self):
        return self.merge_maps(super(OverViewContentItem, self).map, {
            '_sr': {
                'node': {
                    'display_NAME': ('name', str),
                    'itemID': ('item_id', int),
                    'rawDistance': ('distance', float),
                    'selected': ('selected', bool),
                }
            }
        })


class OverViewTabGroupPanelView(tabs.TabGroupPanelInGameObject):
    content_class = OverViewContent


class OverViewTabGroupView(tabs.TabGroupInGameObject):
    job_class = BlockingJob
    job_delay_seconds = 1
    job_repeat = float('inf')

    panel_class = OverViewTabGroupPanelView
