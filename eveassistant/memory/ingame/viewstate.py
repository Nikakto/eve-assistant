from typing import List

from memory.ingame.generic import AbstractInGameObject
from memory.process import EveOnlineProcess


class ViewState(AbstractInGameObject):
    display: bool = False
    name: str = 'Unknown'

    job_repeat = float('inf')
    job_delay_seconds = 1

    @property
    def map(self):
        return {
            '_display': ('display', bool, {self.CHILD_OPTIONAL: True}),
            '_name': ('name', str),
        }


class ViewStates(AbstractInGameObject):
    VIEW_STATE_INFLIGHT = 'l_inflight'

    job_repeat = float('inf')
    job_delay_seconds = 1

    states: List['ViewState']

    def __init__(self, process: 'EveOnlineProcess', node=None, ingame_parent: 'AbstractInGameObject' = None):
        super(ViewStates, self).__init__(process, node, ingame_parent)
        self.states = []

    @property
    def map(self):
        return {
            'children': {
                self.CHILD_LIST: ('states', ViewState)
            }
        }

    @property
    def state(self):
        for state in self.states:
            if state.display:
                return state.name
