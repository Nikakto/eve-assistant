import logging
from typing import List

from .generic import VisibleInGameObject
from .scroll import ScrollAreaInGameObject


logger = logging.getLogger("eve_assistant")


class TabGroupTabDataInGameObject(VisibleInGameObject):
    pass


class TabGroupPanelInGameObject(ScrollAreaInGameObject):
    @property
    def x(self) -> int:
        """Tab panel align relative parent of TabGroup"""
        if self.ingame_parent and isinstance(self.ingame_parent.ingame_parent, VisibleInGameObject):
            return self.ingame_parent.ingame_parent.x + self._x
        logger.warning(f'''{self.__class__.__name__} cannot align by parent of parent''')
        return super(TabGroupPanelInGameObject, self).x


class TabGroupInGameObject(VisibleInGameObject):
    panel_class = TabGroupPanelInGameObject
    tabs_class = TabGroupTabDataInGameObject

    panel: panel_class = None
    tabs: List[tabs_class]

    def __init__(self, *args, **kwargs):
        super(TabGroupInGameObject, self).__init__(*args, **kwargs)
        self.tabs = []

    @property
    def map(self):
        return self.merge_maps(super(TabGroupInGameObject, self).map, {
            'selectedTab': {
                '_sr': {
                    'panel': ('panel', self.panel_class)
                }
            },
            # 'tabData': {
            #     self.CHILD_LIST: ('tabs', self.tabs_class)
            # }
        })

    def update(self) -> None:
        super(TabGroupInGameObject, self).update()
        self.panel.update()
        for tab in self.tabs:
            tab.update()
