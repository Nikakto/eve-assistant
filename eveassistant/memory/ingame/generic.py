import logging
from typing import Callable, List, TYPE_CHECKING

import math
from copy import deepcopy
from memory import nodes, tasks

if TYPE_CHECKING:
    from memory.process import EveOnlineProcess


logger = logging.getLogger("eve_assistant")


class AbstractInGameObject:
    CHILD_OPTIONAL = '$_optional'
    CHILD_KWARGS = '$_kwargs'
    CHILD_LIST = '$_each'

    ingame_parent: 'AbstractInGameObject'
    process: 'EveOnlineProcess'
    node: nodes.Node = None

    job: tasks.Job
    job_class = tasks.Job
    job_priority = tasks.Job.PRIORITY_NORMAL
    job_repeat = 0
    job_delay_seconds = 1

    def __init__(self, process: 'EveOnlineProcess', node: 'nodes.Node', ingame_parent: 'AbstractInGameObject' = None):
        self.node = node
        self.process = process
        self.ingame_parent = ingame_parent

        kwargs = {
            'target': self.update,
            'priority': self.job_priority,
            'delay': self.job_delay_seconds,
            'repeat': self.job_repeat,
        }

        if self.job_class is tasks.BlockingJob:
            kwargs['process'] = self.process

        self.job = self.job_class(**kwargs)
        if self.job_repeat:
            self.job.start()

    def _set_child_simple(self, node, attr, klass: Callable) -> None:
        current_node = getattr(self, attr) if hasattr(self, attr) else None
        if current_node and node and node.address == current_node.node.address:
            return None
        elif current_node:
            current_node.kill()

        value = klass(self.process, node, ingame_parent=self) if klass and node else None
        setattr(self, attr, value)

    def _set_child_list(self, node, attr, klass: Callable) -> None:
        items = getattr(self, attr)
        exists = [item.node for item in getattr(self, attr)]
        children_dead = (set(exists) - set(node.children.values())) if exists else set()
        to_delete = [item for item in items if item.node in children_dead]
        for ingame_object in to_delete:
            ingame_object.kill()
            items.remove(ingame_object)

        to_append = [child_node for child_node in node.children.values() if child_node not in exists]
        for child_node in to_append:
            item = klass(self.process, child_node)
            items.append(item)
            item.update()

    @staticmethod
    def distance_formatted(distance):
        if distance < 10 * 1000 + 1:
            return f'{distance:.2f} m'
        elif distance < 1000 * 1000 * 1000 + 1:
            return f'{distance / 1000:.2f} km'
        else:
            return f'{distance / 149597870700:.2f} au'

    def focus(self, child: 'VisibleInGameObject'):
        pass

    def kill(self):
        self.job.kill()

    @property
    def map(self) -> dict:
        return {}

    def merge_maps(self, x, y):
        if not isinstance(x, dict) or not isinstance(y, dict):
            logger.warning(f'''{self.__class__.__name__} trying to merge <x: {type(x)}> and <y: {type(y)}>''')
            return x

        z = {}
        overlapping_keys = x.keys() & y.keys()
        for key in overlapping_keys:
            z[key] = self.merge_maps(x[key], y[key])
        for key in x.keys() - overlapping_keys:
            z[key] = deepcopy(x[key])
        for key in y.keys() - overlapping_keys:
            z[key] = deepcopy(y[key])
        return z

    def read_map(self, node, subroutes=None) -> None:
        if not subroutes:
            error = f'''{self.__class__.__name__} has no subroutes to read from {node=}'''
            logger.warning(error)
            return

        node.read_children(children_to_read=subroutes.keys())
        for child_name, child_value, in subroutes.items():
            child_node = node.get(child_name)

            if isinstance(child_value, tuple):
                attr, klass, kwargs = child_value if len(child_value) == 3 else (*child_value, {})
                if not klass:
                    continue
            else:
                kwargs = child_value.get(self.CHILD_KWARGS, {})

            if not child_node and child_name is not self.CHILD_LIST:
                if not kwargs.get(self.CHILD_OPTIONAL):
                    error = f'''{self.__class__.__name__} has no {child_name=} to at {node=}'''
                    logger.warning(error)
                continue

            if not isinstance(child_value, tuple):
                self.read_map(child_node, child_value)
            elif child_name == self.CHILD_LIST:
                self._set_child_list(node, attr, klass)
            elif klass in (bool, float, int, str):
                setattr(self, attr, klass(child_node.value))
            else:
                self._set_child_simple(child_node, attr, klass)

    def update(self) -> None:
        self.read_map(self.node, self.map)


class VisibleInGameObject(AbstractInGameObject):
    _x: int = 0
    _y: int = 0
    _height: int = 0
    _width: int = 0

    _content_x: int = 0
    _content_y: int = 0
    _content_height: int = 0
    _content_width: int = 0

    def click(self):
        self.mouse_focus()
        self.process.mouse_click()

    @property
    def content_height(self) -> int:
        return self._content_height

    @property
    def content_x(self) -> int:
        return self.x + self._content_x

    @property
    def content_y(self) -> int:
        return self.y + self._content_y

    @property
    def content_width(self) -> int:
        return self._content_width

    def focus(self, child: 'VisibleInGameObject'):
        self.ingame_parent.focus(child=self)

    @property
    def height(self) -> int:
        return self._height

    @height.setter
    def height(self, value: int):
        self._height = value
        self._content_height = value

    @property
    def map(self):
        return self.merge_maps(super(VisibleInGameObject, self).map, {
            '_displayHeight': ('height', int),
            '_displayX': ('_x', int),
            '_displayY': ('_y', int),
            '_displayWidth': ('width', int),
        })

    def mouse_focus(self):
        self.process.mouse_move(self.x_center, self.y_center)

    @property
    def width(self) -> int:
        return self._width

    @width.setter
    def width(self, value: int):
        self._width = value
        self._content_width = value

    @property
    def x(self) -> int:
        return self.ingame_parent.content_x + self._x if isinstance(self.ingame_parent, VisibleInGameObject) else self._x

    @property
    def x_center(self) -> int:
        return self.content_x + math.floor(self.width / 2)

    @property
    def y(self) -> int:
        return self.ingame_parent.content_y + self._y if isinstance(self.ingame_parent, VisibleInGameObject) else self._y

    @property
    def y_center(self) -> int:
        return self.content_y + math.floor(self.height / 2)


class WindowInGameObject(VisibleInGameObject):

    @property
    def height(self) -> int:
        return self._height

    @height.setter
    def height(self, value: int):
        self._height = value

    @property
    def map(self):
        return self.merge_maps(super(WindowInGameObject, self).map, {
            '_sr': {
                'main': {
                    '_displayHeight': ('_content_height', int),
                    '_displayX': ('_content_x', int),
                    '_displayY': ('_content_y', int),
                    '_displayWidth': ('_content_width', int),
                }
            },
        })

    @property
    def width(self) -> int:
        return self._width

    @width.setter
    def width(self, value: int):
        self._width = value
