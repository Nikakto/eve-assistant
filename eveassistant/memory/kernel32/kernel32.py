import ctypes

from . import types


get_last_error = ctypes.WinDLL('kernel32', use_last_error=True).GetLastError


get_process_memory_info = ctypes.windll.psapi.GetProcessMemoryInfo
get_process_memory_info.restype = ctypes.wintypes.BOOL
get_process_memory_info.argtypes = (
    ctypes.wintypes.HANDLE,
    ctypes.POINTER(types.ProcessMemoryCountersEx),
    ctypes.wintypes.DWORD,
)


get_system_info = ctypes.WinDLL('kernel32', use_last_error=True).GetSystemInfo
get_system_info.restype = None
get_system_info.argtypes = (types.LpSystemInfo, )


read_process_memory = ctypes.WinDLL('kernel32', use_last_error=True).ReadProcessMemory
read_process_memory.restype = ctypes.wintypes.BOOL
read_process_memory.argtypes = (
    ctypes.wintypes.HANDLE,
    ctypes.c_ulonglong,
    ctypes.wintypes.LPVOID,
    ctypes.c_size_t,
    ctypes.POINTER(ctypes.c_size_t)
)


set_last_error = ctypes.WinDLL('kernel32', use_last_error=True).SetLastError


virtual_query_ex = ctypes.WinDLL('kernel32', use_last_error=True).VirtualQueryEx
virtual_query_ex.argtypes = (
    ctypes.wintypes.HANDLE,
    ctypes.c_ulonglong,
    ctypes.POINTER(types.MemoryBasicInformation),
    ctypes.c_ulonglong,
)
