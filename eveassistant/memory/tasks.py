import logging
import psutil
import queue
import time
import traceback
from threading import Event, Thread
from typing import Callable

from memory.process import EveOnlineProcess

logger = logging.getLogger("eve_assistant")


class Job:
    PRIORITY_HIGH = 0
    PRIORITY_NORMAL = 1
    PRIORITY_LOW = 2

    def __init__(self, target: Callable, priority: int = PRIORITY_NORMAL, delay=1, repeat=0):
        self.delay = delay
        self.priority = priority
        self.repeat = repeat
        self.target = target

    def __lt__(self, other: 'Job'):
        return self.priority < other.priority

    def __eq__(self, other):
        return self.priority == other.priority

    def _repeat(self):
        self.repeat -= 1
        time.sleep(self.delay)
        self.start()

    def _run(self):
        try:
            self.target()
        except Exception:
            tb = traceback.format_exc()
            logger.error(tb)
            traceback.print_exc()
            return

    def kill(self):
        self.repeat = 0

    def run(self):
        self._run()
        if self.repeat:
            Thread(target=self._repeat).start()

    def start(self):
        default_worker.put(self)


class BlockingJob(Job):
    def __init__(self, process: 'EveOnlineProcess', *args, **kwargs):
        super(BlockingJob, self).__init__(*args, **kwargs)
        self.process = psutil.Process(process.pid)

    def run(self):
        self.process.suspend()
        try:
            super(BlockingJob, self).run()
        except Exception as error:
            raise error
        finally:
            self.process.resume()


class Worker(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *, daemon=None):
        super(Worker, self).__init__(group, target, name, args, kwargs, daemon=daemon)
        self.queue = queue.PriorityQueue()

        self._kill = Event()
        self._pause = Event()

    def kill(self):
        self._kill.set()

    def put(self, job: Job):
        self.queue.put(job)

    def pause(self):
        self._pause.set()

    def resume(self):
        if self.is_alive():
            self._pause.clear()
        else:
            logger.error('Trying to resume killed task runner')

    def run(self) -> None:
        while not self._kill.is_set():
            if self._pause.is_set():
                time.sleep(1)
            elif self.queue.empty():
                time.sleep(0.1)
            else:
                # time_start = time.time()
                task = self.queue.get()
                task.run()
                self.queue.task_done()
                # print(f'Job finished: {time.time() - time_start:.2f} ({task.target})')


default_worker = Worker()
default_worker.start()


