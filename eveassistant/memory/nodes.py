import logging
from typing import Dict, Iterable, Tuple, Union, TYPE_CHECKING


if TYPE_CHECKING:
    from memory.process import EveOnlineProcess


logger = logging.getLogger("eve_assistant")


class Node:
    TYPE_BOOL = 'bool'
    TYPE_BUNCH = 'Bunch'
    TYPE_DICT = 'dict'
    TYPE_INT = 'int'
    TYPE_FLOAT = 'float'
    TYPE_LIST = 'list'
    TYPE_LONG = 'long'
    TYPE_PY_CHIDLREN_LIST = 'PyChildrenList'
    TYPE_STR = 'str'
    TYPE_TUPLE = 'tuple'
    TYPE_UIROOT = 'UIRoot'
    TYPE_UNICODE = 'unicode'

    def __init__(self, process: 'EveOnlineProcess', address: int, *args, **kwargs):
        super(Node, self).__init__(*args, **kwargs)

        self.address = address
        self.process = process

        self._children = {}
        self._value = None

    def __repr__(self) -> str:
        return f'''<{self.__class__.__name__}: (pid={self.process.pid}; address={hex(self.address).upper()})>'''

    @staticmethod
    def _child_is_updated(node, node_prev=None):
        if not node:
            return False
        if not node_prev:
            return True
        return node.address != node_prev.address or node.size != node_prev.size

    @staticmethod
    def _read_node_type(process, address) -> Union[str, None]:
        return process.read_type(address)

    @property
    def address_hex(self):
        return hex(self.address).upper() if self.address else ''

    def cannot_read_class_value_at(self):
        logger.warning(f'''Cannot read value for class={self.__class__=} at address={self.address}''')

    @property
    def children(self) -> Dict[str, 'Node']:
        if not self._children:
            self.read_children()
        return self._children

    def get(self, child) -> Union['Node', None]:
        return self.children.get(child, None)

    def get_children(self, children_to_read: Union[Iterable, None]) -> Dict[str, 'Node']:
        return {}

    @classmethod
    def from_address(cls, process: 'EveOnlineProcess', address: int):
        if cls is not Node:
            return cls(process, address)

        node_type = cls._read_node_type(process, address)
        if node_class := MAP_TYPE_TO_CLASS.get(node_type):
            return node_class(process, address)
        else:
            logger.info(f'''Unknown {node_type=} at {address=}''')
            return NodeBase(process, address)

    def has_children(self):
        if isinstance(self, NodeBase):
            return bool(self.size)
        else:
            return type(self) in (NodeDict, NodeList, NodePyChildrenList, NodeTuple) and self.size

    def read_children(self, children_to_read: Union[Iterable, None] = None):
        children = self.get_children(children_to_read=children_to_read)

        for key, node in children.items():
            node_prev = self._children.get(key)
            if not self._child_is_updated(node, node_prev):
                children[key] = node_prev

        if set(self._children.values()) ^ set(children.values()):
            self._children = children

    def read_node_type(self):
        return self._read_node_type(self.process, self.address)

    @property
    def size(self) -> Union[int, None]:
        return 0

    @property
    def type(self) -> Union[str, None]:
        return self.read_node_type()

    def update_children(self) -> Dict[str, 'Node']:
        self._children = {}
        return self.children

    @property
    def value(self):
        return ''


class NodeBase(Node):
    _dict: Union['NodeDict', None] = None
    PROP_NAME = '_name'

    def _get_dict(self) -> Union['NodeDict', None]:
        address = self.process.read_uint64(self.address + 2 * self.process.WORD_SIZE)
        _dict = Node.from_address(self.process, address) if address else None
        if _dict and _dict.type == Node.TYPE_DICT:
            return self._dict if self._dict and self._dict.address == _dict.address else _dict
        return self._dict

    @property
    def dict(self) -> Union['NodeDict', None]:
        if self._dict is None:
            self._dict = self._get_dict()
        return self._dict

    def get_children(self, children_to_read: Union[Iterable, None] = None) -> Dict[str, 'Node']:
        self._dict = None
        if not self.dict:
            return {}
        _children_to_read = [*children_to_read, self.PROP_NAME] if children_to_read else [self.PROP_NAME]
        self.dict.read_children(children_to_read=children_to_read)
        return self.dict.children

    @property
    def name(self):
        node_str = self.children.get(self.PROP_NAME)
        return node_str.value if node_str else None

    @property
    def size(self) -> Union[int, None]:
        return self.dict.size if self.dict else 0


class NodeBool(Node):
    _value: bool

    @property
    def value(self) -> bool:
        if self._value is None:
            value = self.process.read_bool(self.address + 2 * self.process.WORD_SIZE)
            if value is None:
                self.cannot_read_class_value_at()
                return False
            else:
                self._value = value
        return self._value


class NodeDict(Node):
    def __init__(self, *args, **kwargs):
        super(NodeDict, self).__init__(*args, **kwargs)
        self.size_slot = self.process.WORD_SIZE * 3

    def _get_item(self, slot_data, children_to_read: Union[Iterable, None] = None) -> Union[Tuple[str, Union['Node', None]], None]:
        _hash = int.from_bytes(slot_data[0:self.process.WORD_SIZE], byteorder='little')
        if not _hash:
            return None

        addressKey = int.from_bytes(slot_data[self.process.WORD_SIZE:2*self.process.WORD_SIZE], byteorder='little')
        keyNode = Node.from_address(self.process, addressKey) if addressKey else None
        if children_to_read is not None and keyNode and keyNode.value not in children_to_read:
            return keyNode and str(keyNode.value), None

        addressValue = int.from_bytes(slot_data[2*self.process.WORD_SIZE:3*self.process.WORD_SIZE], byteorder='little')
        valueNode = Node.from_address(self.process, addressValue) if keyNode and addressValue else None
        return keyNode and str(keyNode.value), valueNode

    def get_children(self, children_to_read: Union[Iterable, None] = None) -> Dict[str, 'Node']:
        children = {}
        ma_loc, size = self.ma_loc, self.size
        if ma_loc is None or size is None:
            logger.error(f'''Cannot read dict at {self.address=} ({ma_loc=}, {size=})''')
        else:
            shift = 0
            while len(children) < size:
                slots_to_read = size - len(children)
                _bytes = self.process.read_bytes(ma_loc + shift, slots_to_read * self.size_slot)
                slots = [_bytes[index*self.size_slot: (index+1)*self.size_slot] for index in range(slots_to_read)]
                for slot_data in slots:
                    if item := self._get_item(slot_data, children_to_read=children_to_read):
                        key, value = item
                        children[key] = value

                shift += slots_to_read * self.size_slot
        return {key: value for key, value in children.items() if value}

    @property
    def ma_loc(self) -> Union[int, None]:
        return self.process.read_uint64(self.address + 5 * self.process.WORD_SIZE)

    @property
    def size(self) -> Union[int, None]:
        return self.process.read_uint64(self.address + 2 * self.process.WORD_SIZE)


class NodeFloat(Node):
    _value: float

    @property
    def value(self) -> float:
        if not self._value:
            self._value = self.process.read_float(self.address + 2 * self.process.WORD_SIZE)
            if self._value is None:
                self.cannot_read_class_value_at()
        return self._value or 0


class NodeInt(Node):
    _value: int
    INT32_MAX_VALUE = 0x7FFFFFFF
    UINT32_MAX_VALUE = 0xFFFFFFFF

    @property
    def value(self) -> int:
        if not self._value:
            self._value = self.process.read_uint32(self.address + 2 * self.process.WORD_SIZE)
            if self._value is None:
                self.cannot_read_class_value_at()
        return self._value or 0

    @classmethod
    def as_value_int32(cls, value: int) -> int:
        return value - cls.UINT32_MAX_VALUE if value > cls.INT32_MAX_VALUE else value


class NodeList(Node):
    def _get_item(self, address) -> Tuple[str, Union['Node', None]]:
        node = Node.from_address(self.process, address)
        node_name = f'{node.type} [{node.address_hex}]'
        if isinstance(node, NodeBase):
            node_name = str(node.name or node_name)
        return node_name, node

    def get_children(self, children_to_read: Union[Iterable, None] = None) -> Dict[str, Union['Node', None]]:
        children = {}
        ma_loc, size = self.ma_loc, self.size
        if ma_loc is None or size is None:
            logger.error(f'''Cannot read list at {self.address=} ({ma_loc=}, {size=})''')
        elif _bytes := self.process.read_bytes(ma_loc, size * self.process.WORD_SIZE):
            addresses = (int.from_bytes(_bytes[index*self.process.WORD_SIZE:(index+1)*self.process.WORD_SIZE], byteorder='little') for index in range(size))
            for address in addresses:
                key, value = self._get_item(address)
                if value and (not children_to_read or key in children_to_read):
                    children[key] = value
        return children

    @property
    def ma_loc(self) -> Union[int, None]:
        return self.process.read_uint64(self.address + 3 * self.process.WORD_SIZE)

    @property
    def size(self) -> Union[int, None]:
        return self.process.read_uint64(self.address + 2 * self.process.WORD_SIZE)


class NodePyChildrenList(NodeBase):
    def get_children(self, children_to_read: Union[Iterable, None] = None) -> Dict[str, 'Node']:
        children = super(NodePyChildrenList, self).get_children(children_to_read=['_childrenObjects'])
        if child_node := children.get('_childrenObjects'):
            child_node.read_children(children_to_read=children_to_read)
            return child_node.children
        return {}


class NodeStr(Node):
    _value: str

    @property
    def size(self) -> int:
        return self.process.read_uint64(self.address + 2 * self.process.WORD_SIZE)

    @property
    def value(self) -> str:
        if not self._value:
            length = self.size
            value = self.process.read_str(self.address + 4 * self.process.WORD_SIZE, length) if length else None
            if length is None or value is None:
                self.cannot_read_class_value_at()
            self._value = value if length and value else ''
        return self._value


class NodeTuple(NodeList):
    @property
    def ma_loc(self) -> int:
        return self.address + 3 * self.process.WORD_SIZE


class NodeUIRoot(NodeBase):
    def __init__(self, *args, **kwargs):
        from memory.ingame import UIRoot
        super(NodeUIRoot, self).__init__(*args, **kwargs)
        self.ingame = UIRoot(self.process, self)


class NodeUnicode(Node):
    @property
    def ma_loc(self) -> Union[int, None]:
        return self.process.read_uint64(self.address + 3 * self.process.WORD_SIZE)

    @property
    def size(self) -> int:
        return self.process.read_uint64(self.address + 2 * self.process.WORD_SIZE)

    @property
    def value(self) -> str:
        if not self._value:
            length = self.size
            value = self.process.read_unicode(self.ma_loc, length) if length else None
            if length is None or value is None:
                self.cannot_read_class_value_at()
            self._value = value if length and value else ''
        return self._value


MAP_TYPE_TO_CLASS = {
    Node.TYPE_BOOL: NodeBool,
    Node.TYPE_BUNCH: NodeDict,
    Node.TYPE_DICT: NodeDict,
    Node.TYPE_FLOAT: NodeFloat,
    Node.TYPE_INT: NodeInt,
    Node.TYPE_LIST: NodeList,
    Node.TYPE_LONG: NodeInt,
    Node.TYPE_PY_CHIDLREN_LIST: NodePyChildrenList,
    Node.TYPE_STR: NodeStr,
    Node.TYPE_TUPLE: NodeTuple,
    Node.TYPE_UIROOT: NodeBase,  # NodeUIRoot init only at process
    Node.TYPE_UNICODE: NodeUnicode,
}
