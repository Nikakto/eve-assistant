import evemem
import logging
import psutil
import struct
import win32api
import win32con
import win32gui
import win32process
from typing import List, Tuple, Union

import itertools
import os
import time

from core.exceptions import Kernel32Error
from memory import kernel32, nodes

logger = logging.getLogger("eve_assistant")


SM_CXPADDEDBORDER = 92
SM_CYBORDER = 6
SM_CYCAPTION = 4
SM_CYFRAME = 33


class EveOnlineProcess:
    __pids = set()
    __processes = []
    __types = {}

    charname: str
    process: object
    uiroot: Union[nodes.NodeUIRoot, None]

    WORD_SIZE = 8

    def __init__(self, pid):
        self.closed = False
        self.hwnd = None
        self.uiroot = None

        self.system_info = kernel32.get_system_info()

        self.pid = pid
        self.__define_hwnd()
        if not self.hwnd:
            logger.info(f'''EveOnline process pid={self.pid} has not visible window''', )
            self.pid = 0

        self.charname = self._get_charname() if self.hwnd else ''
        self.process = win32api.OpenProcess(kernel32.PROCESS_ALL_ACCESS, 0, self.pid) if self.pid else None
        self._process_c = evemem.EveOnlineProcess(self.pid)

        if os.environ.get('UIROOT'):
            self.uiroot = nodes.NodeUIRoot(self, int(os.environ.get('UIROOT'), 16))

    def __repr__(self):
        return f'''EveOnlineProcess (pid={self.pid})'''

    def __hwnd_set_on_match_pid(self, hwnd, pid):
        _, hwnd_pid = win32process.GetWindowThreadProcessId(hwnd)
        if hwnd_pid == pid and win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
            self.hwnd = hwnd

    def __define_hwnd(self):
        win32gui.EnumWindows(self.__hwnd_set_on_match_pid, self.pid)

    def _get_charname(self) -> str:
        return win32gui.GetWindowText(self.hwnd).replace('EVE - ', '')

    def close(self):
        self.process.close()
        self.closed = True

    def find_uiroot_class_name(self):
        addresses = self.find_value(b'UIRoot\x00')

        # remove all non octet address
        addresses = [address for address in addresses if not address % self.WORD_SIZE]

        # WARN!!! risky filter:
        # I believe CCP use UIRoot.__class__.__name__ as dict key.
        # So, key in dict is address of str's object belong to UIRoot.__class__.__name__
        addresses = [address for address in addresses if 'str' == self.read_type(address - 4 * self.WORD_SIZE)]
        return addresses

    def find_uiroot_instance(self, addresses: List[int]):
        temp = [self.find_value(address.to_bytes(self.WORD_SIZE, byteorder='little')) for address in addresses]
        temp = list(itertools.chain(*temp))

        for address in temp:
            node = nodes.Node.from_address(self, address - self.WORD_SIZE)
            if not node:
                continue
            node_name = node.children.get('_name')
            if not node_name or node_name.type != nodes.Node.TYPE_STR:
                continue
            if node_name.value == 'Desktop':
                self.uiroot = nodes.NodeUIRoot(self, node.address)
                return

    def find_uiroot_class_type(self, addresses: List[int]):
        result = [self.find_value(address.to_bytes(self.WORD_SIZE, byteorder='little'), limit=1) for address in addresses]
        result = [address - 24 for address in itertools.chain(*result)]
        return [address for address in result if 'type' == self.read_type(address)]

    def find_value(self, value: bytes, limit=100):
        address = self.system_info.lpMinimumApplicationAddress

        result = []
        while address < self.system_info.lpMaximumApplicationAddress:
            memory_info = kernel32.virtual_query_ex(self.process.handle, address)
            if memory_info.accessible and memory_info.State == kernel32.MEM_COMMIT:
                dump = self.read_bytes(memory_info.BaseAddress, memory_info.RegionSize)
                index = -1
                while (index := dump.find(value, index + 1)) > 0:
                    result.append(address + index)
                    if len(result) >= limit:
                        logger.info(f'''{self.__class__.__name__} find too much {value=}. Stop memory scan''')
                        return result
            address += memory_info.RegionSize
        return result

    @classmethod
    def get_processes_all(cls) -> List['EveOnlineProcess']:
        cls.get_processes_diff()
        return cls.__processes

    @classmethod
    def get_processes_diff(cls) -> Tuple[List['EveOnlineProcess'], List['EveOnlineProcess']]:
        pids = set([process.pid for process in psutil.process_iter() if process.name() == 'exefile.exe'])

        processes_new = [EveOnlineProcess(pid=pid) for pid in pids - cls.__pids]
        processes_dead = [process for process in cls.__processes if process.pid in cls.__pids - pids]

        cls.__processes += processes_new
        cls.__processes = [process for process in cls.__processes if process.pid in pids]
        cls.__pids = set([process.pid for process in cls.__processes])

        processes_new = [process for process in processes_new if process in cls.__processes]
        return processes_new, processes_dead

    @property
    def ingame_pos(self) -> Tuple[int, int]:
        left1, top1, right1, bottom1 = win32gui.GetWindowRect(self.hwnd)
        left2, top2, right2, bottom2 = win32gui.GetClientRect(self.hwnd)

        caption_height = (win32api.GetSystemMetrics(SM_CYFRAME) + win32api.GetSystemMetrics(SM_CYCAPTION) + win32api.GetSystemMetrics(SM_CXPADDEDBORDER));
        border_size = win32api.GetSystemMetrics(SM_CYBORDER)

        x = left1 + border_size + ((right1 - left1) - right2) / 2
        y = top1 + caption_height
        return x, y

    def mouse_click(self):
        win32api.SendMessage(self.hwnd, win32con.WM_LBUTTONDOWN, 0, 0)
        time.sleep(0.1)
        win32api.SendMessage(self.hwnd, win32con.WM_LBUTTONUP, 0, 0)
        time.sleep(0.1)

    def mouse_click_at_pos(self, x, y):
        self.mouse_move(x, y)
        self.mouse_click()

    def mouse_move(self, x, y):
        lParam = win32api.MAKELONG(x, y)
        win32api.SendMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, lParam)
        time.sleep(0.1)

    def mouse_wheel(self, delta_lines=0):
        win32api.SendMessage(self.hwnd, win32con.WM_MOUSEWHEEL, win32api.MAKELONG(0, delta_lines * win32con.WHEEL_DELTA), 0)
        time.sleep(0.1)

    def read_bool(self, address) -> Union[bool, None]:
        """"
        :param address: address to read
        :return: on success: bool; on error: None;
        """
        value = self.read_bytes(address, 4)
        return (0 != int.from_bytes(value, byteorder='little')) if value is not None else None

    def read_bytes(self, address, length) -> Union[bytes, None]:
        """"
        :param address: address to read
        :param length: bytes to read
        :return: on success: bytes; on error: None;
        """
        if address < self.system_info.lpMaximumApplicationAddress:
            # memory_info = kernel32.virtual_query_ex(self.process.handle, address)
            # if memory_info.accessible and memory_info.State == kernel32.MEM_COMMIT:
            #     return kernel32.read_process_memory(self.process.handle, address, length)
            # else:
            #     logger.warning(f'Trying to read inaccessible memory {address=}')
            try:
                return self._process_c.read_bytes(address, length)
                # return kernel32.read_process_memory(self.process.handle, address, length)
            except Kernel32Error:
                logger.warning(f'Memory reading error with {address=}')
        return None

    def read_float(self, address) -> Union[float, None]:
        """"
        :param address: address to read
        :return: on success: float; on error: None;
        """
        value = self.read_bytes(address, 8)
        return struct.unpack('d', value)[0] if value is not None else None

    def read_int32(self, address) -> Union[int, None]:
        """"
        :param address: address to read
        :return: on success: int; on error: None;
        """
        value = self.read_bytes(address, 4)
        return int.from_bytes(value, byteorder='little', signed=True) if value is not None else None

    def read_int64(self, address) -> Union[int, None]:
        """"
        :param address: address to read
        :return: on success: int; on error: None;
        """
        value = self.read_bytes(address, self.WORD_SIZE)
        return int.from_bytes(value, byteorder='little', signed=True) if value is not None else None

    def read_str(self, address, length) -> Union[str, None]:
        """"
        :param address: address to read
        :param length: string length to read
        :return: on success: str on error: None;
        """
        value = self.read_bytes(address, length)
        return value.decode() if value is not None else None

    def read_type(self, address) -> Union[str, None]:
        return self._process_c.read_type(address)
        # """"
        # :param address: address to read
        # :return: on success: str; on error: None;
        # """
        #
        # address_of_type = self.read_int64(address + self.WORD_SIZE)
        # if address_of_type is None:
        #     return None
        #
        # if self.__types.get(address_of_type):
        #     return self.__types[address_of_type]
        #
        # address_of_type_repr = self.read_int64(address_of_type + 3 * self.WORD_SIZE)
        # if address_of_type_repr is None:
        #     return None
        #
        # for size in range(1, 32):
        #     type_repr = self.read_bytes(address_of_type_repr, size)
        #     if type_repr is None or not type_repr.isascii():
        #         return None
        #
        #     if b'\00' in type_repr:
        #         type_name = type_repr[:-1].decode()
        #         self.__types[address_of_type] = type_name
        #         return type_name
        # return None

    def read_uint32(self, address) -> Union[int, None]:
        """"
        :param address: address to read
        :return: on success: int; on error: None;
        """
        value = self.read_bytes(address, 4)
        return int.from_bytes(value, byteorder='little') if value is not None else None

    def read_uint64(self, address) -> Union[int, None]:
        """"
        :param address: address to read
        :return: on success: int; on error: None;
        """
        value = self.read_bytes(address, self.WORD_SIZE)
        return int.from_bytes(value, byteorder='little') if value is not None else None

    def read_unicode(self, address, length) -> Union[str, None]:
        """"
        :param address: address to read
        :param length: string length to read
        :return: on success: str on error: None;
        """
        value = self.read_bytes(address, length * 2)
        return value.decode('utf-16') if value is not None else None

    def set_cursor_pos(self, mouse_x, mouse_y):
        lParam = win32api.MAKELONG(mouse_x, mouse_y)
        # win32gui.ShowWindow(self.hwnd, win32con.SW_SHOWNOACTIVATE)
        # time.sleep(1)
        win32gui.ShowWindow(self.hwnd, win32con.SW_MAXIMIZE)
        time.sleep(1)
        win32api.SendMessage(self.hwnd, win32con.WM_MOUSEMOVE, 0, lParam)
        time.sleep(0.1)
        # win32api.SendMessage(self.hwnd, win32con.WM_LBUTTONDOWN, 0, 0)
        # time.sleep(0.1)
        # win32api.SendMessage(self.hwnd, win32con.WM_LBUTTONUP, 0, 0)

    def send_message(self, *args, **kwargs):
        win32api.SendMessage(*args, **kwargs)

